'use strict';

var app = angular.module('mvApp', [
  'ngAnimate',
  'ngCookies',
  'ngResource',
  'ngRoute',
  'ngSanitize',
  'ngTouch'
]).config(['$routeProvider', function ($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: '/index/main',
    controller: 'MainCtrl'
  })
    .when('/index/tools', {
      templateUrl: '/index/tools',
      controller: 'ToolsCtrl'
    })
    .when('/:controller/:action', {
      templateUrl: function (urlAttr) {
        return '/' + urlAttr.controller + '/' + urlAttr.action;
      },
      controller: 'MainCtrl'
    })
    .otherwise({
      redirectTo: '/'
    });
}]);
