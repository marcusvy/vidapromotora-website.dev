'use strict';

angular.module('mvApp')
  .controller('ToolsCtrl', ['$scope','$http',
    function ($scope,$http) {
    $scope.secferramenta = [];

    $http.get('/tools/list').
      success(function(data, status, headers, config) {
        $scope.secferramentas = data.secferramentas;
      }).
      error(function(data, status, headers, config) {
        console.log(data);
      });
  }]);
