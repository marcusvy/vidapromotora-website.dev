'use strict';

angular.module('mvApp')
  .directive('mvSectionTitle', ['MenuService',
    function (MenuService) {
      return {
        restrict: 'C',
        template: '{{scope.page}}',
        link: function(scope,element,attrs){
          scope.page = 'abacate';
          scope.$on('MenuService.setPage', function ($event) {
            scope.page = MenuService.getPage();
          });
        }
      };
    }]);
