'use strict';

angular.module('mvApp')
  .directive('btMenu', ['MenuService',
    function (MenuService) {
    return {
      restrict: 'C',
      controller: function($scope,$element,$attrs){
        $scope.status = MenuService.getStatus();

        $scope.open = function($event){
          $event.preventDefault();
          MenuService.toggleStatus();
        };

        $scope.$on('MenuService.toggleStatus',function($event){
          $scope.status = MenuService.getStatus();
        });

      },
      link: function(scope,element,attrs){

      }
    };
  }]);
