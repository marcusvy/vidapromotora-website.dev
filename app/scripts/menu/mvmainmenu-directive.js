'use strict';

angular.module('mvApp')
  .directive('mvMainMenu', ['MenuService', '$rootScope','$location',
    function (MenuService, $rootScope, $location) {
      return {
        restrict: 'C',
        controller: function ($scope, $element, $attrs) {
          $scope.status = MenuService.getStatus();

          $scope.updateActiveClass = function () {
            var menuItem = $element.find('.menu').children().children();
            var url = $location.url();

            angular.forEach(menuItem, function (item) {
              var item = angular.element(item);
              var href = item.attr('href');

              if(item.hasClass('active')){
                item.removeClass('active');
              }
              if(href.substr(1,href.length)===url){
                item.addClass('active');
                MenuService.setPage(item[0].innerText);
              }
            });
          };
          $scope.$on(['MenuService.toggleStatus', 'MenuService.setStatus'], function ($event) {
            $scope.status = MenuService.getStatus();
          });

        },
        link: function (scope, element, attr) {

          $rootScope.$on('$routeChangeSuccess', function () {
            if (scope.status) {
              MenuService.toggleStatus();
            }
            scope.updateActiveClass();
          });
        }

      };
    }]);
