'use strict';

angular.module('mvApp')
  .service('MenuService', ['$rootScope',
    function ($rootScope) {

      /**
       * Abertura do menu
       * @type {boolean}
       */
      this.status = false;

      /**
       * Título da Página
       * @type {string}
       */
      this.page = '';

      /**
       * Obtem o atual estado do menu
       * @returns {boolean}
       */
      this.getStatus = function () {
        return !!this.status;
      };

      /**
       * Altera o menu
       * @param {boolean} status Valor para status
       * @returns {status|*}
       */
      this.setStatus = function (status) {
        this.status = status;
        return $rootScope.$broadcast('MenuService.setStatus');
      };

      /**
       * Altera a chave do estado
       * @returns {status|*}
       */
      this.toggleStatus = function () {
        this.status = !this.status;
        return $rootScope.$broadcast('MenuService.toggleStatus');
      }

      /**
       * Obtem o nome da página atual
       * @returns {string}
       */
      this.getPage = function () {
        return this.page;
      }

      /**
       * Altera o nome da página atual
       * @param page
       * @returns {*|Object}
       */
      this.setPage = function (page) {
        this.page = page;
        return $rootScope.$broadcast('MenuService.setPage');
      }

    }]);
