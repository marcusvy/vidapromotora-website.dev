MV App ZF2
==============

Arquivos iniciais para um projeto baseado em Zend Framework 2.


Configuração do FrontEnd
------------------------
É necessário utilizar o sass para tal recomenda-se instalar o compass

    gem install compass
    
Utilizasse o gulp mas antes é necessário instalar algumas dependências

    npm install -g bower gulp
    npm install
    bower install
    
Inicializar é fácil

    gulp build
    gulp

Configuração do Servidor
------------------------

### PHP CLI Server

Instale primeiro as bibliotecas a partir do Composer:

    php composer.phar self-update
    php composer.phar install

O modo simples de iniciar é usando o PHP 5.4 ou superior para iniciar um servidor PHP no diretório raiz:

    php -S 0.0.0.0:8001 -t public/ public/index.php

Este comando irá iniciar um cli-server na porta 8001, e adicionaná em todas as interfaces de rede.

**Nota: ** O servidor embutido do php é *para desenvolvimento somente*.


Configuração do Aplicativo
--------------------------

Se desejar conectar ao banco de dados, apenas informe no arquivo config/autoload/local.php, o seu usuário e
senha utilizada pelo sistema.

    return array(
        'mv' => array(
            'mv_db' => array(
                'username' => '_usuario_',
                'password' => '_senha_',
            )
        )
    );

**Não esqueça de verificar se o arquivo 'config/autoload/global.php' está de acordo com as configurações
do seu servidor.**

Acessibilidade
--------------------------
O termo acessibilidade significa incluir a pessoa com deficiência na participação de atividades como o uso de produtos, serviços e informações. Alguns exemplos são os prédios com rampas de acesso para cadeira de rodas e banheiros adaptados para deficientes.
Na internet, acessibilidade refere-se principalmente às recomendações do WCAG (World Content Accessibility Guide) do W3C e no caso do Governo Brasileiro ao e-MAG (Modelo de Acessibilidade em Governo Eletrônico). O e-MAG está alinhado as recomendações internacionais, mas estabelece padrões de comportamento acessível para sites governamentais.
O projeto mvCosmos tenta ao máximo utilizar esses conceitos para que o sistema esteja sevindo a todos.

## Autor

**Marcus Vinícius Cardoso**

- <http://mviniciusconsultoria.com.br>

## Direitos autorais

-  [Angular JS](https://github.com/angular/angular.js/blob/master/LICENSE)
-  [Bootstrap](https://github.com/twbs/bootstrap-sass/blob/master/LICENSE)
-  [Bower](https://github.com/bower/bower/blob/master/LICENSE)
-  [Gulp](https://github.com/gulpjs/gulp/blob/master/LICENSE)
-  [JQuery](https://jquery.org/license/)
-  [Zend Framework 2](https://github.com/zendframework/zf2/blob/master/LICENSE.txt)

Código e documentação direitos reservados 2013-2014, MVinicius Consultoria, lançado com [licença CCO 1.0 Universal](LICENSE).
Documentação sobre licença [Creative Commons](docs/LICENSE).

