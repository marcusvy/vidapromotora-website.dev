'use strict';

/**
 *  Tarefas comuns
 */
var gulp = require('gulp');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var wiredep = require('wiredep').stream;

var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'del', 'main-bower-files']
});

var appSettings = require('./config.json').appSettings;
var dirDev = appSettings.directory.dev; //app
var dirApp = appSettings.directory.app; //www/app
//var buildPhp = appSettings.buildPhp;

function handleError(err) {
  console.error(err.toString());
  this.emit('end');
}

//== Limpeza
gulp.task('clean', function () {
  $.del(['.tmp', '.sass-cache']);
});

gulp.task('clean:core', function () {
  $.del([dirApp]);
});

//== Ruby Sass
gulp.task('sass', function () {
  return gulp.src(dirDev + 'styles/**/*.scss')
    .pipe($.sass())
    .on('error', handleError)
    .pipe(gulp.dest(dirDev+ 'styles/'))
    .pipe($.size());
});

gulp.task('styles', function () {
  return gulp.src(dirDev + 'styles/main.css')
    .pipe($.autoprefixer('last 1 version'))
    .pipe($.minifyCss({keepSpecialComments: 0}))
    .pipe(gulp.dest(dirApp+ 'styles/'))
    .pipe(reload({stream: true}))
    .pipe($.size());
});

//== Wiredep
gulp.task('bower', function () {
  return gulp.src(dirDev + 'index.html')
    .pipe(wiredep({
      directory: dirDev + 'bower_components',
      ignorePath: dirDev
    }))
    .pipe(gulp.dest(dirDev))
    .pipe(reload({stream: true}));
});

//== JSHint e Scripts
gulp.task('scripts', function () {
  // .pipe($.ngAnnotate())
  return gulp.src(dirDev + 'scripts/**/*.js')
    .pipe($.concat('scripts/scripts.js'))
    .pipe($.ngAnnotate())
    .pipe($.uglify())
    .pipe(gulp.dest(dirApp))
    .pipe(reload({stream: true}))
    .pipe($.size());
});

gulp.task('jsHint', function () {
  return gulp.src(dirDev + 'scripts/**/*.js')
    .pipe($.jshint())
    .pipe($.jshint.reporter('jshint-stylish'))
    .pipe($.size());
});

//== partials
gulp.task('partials', function () {
  return gulp.src(dirDev + 'partials/**/*.html')
    .pipe($.angularHtmlify())
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe($.ngHtml2js({
      moduleName: appSettings.moduleName,
      prefix: 'partials/'
    }))
    .pipe(gulp.dest('.tmp/partials'))
    .pipe($.size());
});

gulp.task('partialsConcat', function () {
  return gulp.src('.tmp/partials/**/*.js')
    .pipe($.concat('scripts/template.js'))
    .pipe($.ngAnnotate())
    .pipe($.uglify())
    .pipe(gulp.dest(dirDev))
    .pipe($.size());
});


gulp.task('ngDirectives',function(){
  var tcOptions = {
    module : 'mvApp'
  };
  gulp.src(dirDev + 'views/directives/**/*.html')
    .pipe($.angularTemplatecache(tcOptions))
    .pipe(gulp.dest(dirDev + 'scripts/'))
    .pipe(browserSync.reload({stream: true}));
});

//== Injeção do aplicativo no index.html
gulp.task('injectJs', ['partials', 'partialsConcat'], function () {
  var options = {
    name: 'myApp',
    addRootSlash: false,
    ignorePath: [dirDev]
  };

  var sources = gulp.src(dirDev + 'scripts/**/*.js', {
    read: false
  }).pipe($.print());

  return gulp.src(dirDev + 'index.html')
    .pipe($.inject(sources, options))
    .pipe(gulp.dest(dirDev))
    .pipe(reload({stream: true}));
});

//== Usemin : gera diretório www/core
gulp.task('html', function () {
  return gulp.src('./' + dirDev + 'index.html')
    .pipe($.usemin({
      css: [$.minifyCss(), $.autoprefixer('last 1 version'), $.csso()],
      //html: [minifyHtml({empty: true})],
      js: [$.ngAnnotate(),$.uglify()]
    }))
    .pipe(gulp.dest(dirApp))
    .pipe(reload({stream: true}));
});

//== Imagens: otimização
gulp.task('images', function () {
  return gulp.src(dirDev + 'images/**/*.{png,jpg,gif}')
    .pipe($.cache.clear())
    .pipe($.cache($.imagemin({
      optimizationLevel: 3,
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest(dirApp + 'images'))
    .pipe($.size());
});

//== Bower Fonts
gulp.task('fontsBower', function () {
  return gulp.src($.mainBowerFiles())
    .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
    .pipe($.flatten())
    .pipe(gulp.dest(dirApp + 'fonts/bower'))
    .pipe($.size());
});

//== Fontes normais
gulp.task('fontsApp', function () {
  return gulp.src(dirDev + 'fonts/**/*.{eot,svg,ttf,woff}')
    .pipe(gulp.dest(dirApp + 'fonts'))
    .pipe($.size());
});

gulp.task('fonts', ['fontsBower', 'fontsApp']);

gulp.task('build', ['bower','styles','injectJs','html', 'images', 'fonts']);
