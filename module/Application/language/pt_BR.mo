��          �   %   �      0  0   1  !   b  !   �  "   �  0   �  ,   �      '     H     ]     t  
   �  '   �     �     �     �     �     �  2   �  M   0  .   ~  <   �  9   �     $  �  3  9   �  &   7  &   ^  &   �  5   �  -   �  #        4     H     _     o  &   {     �     �     �     �     �  7   �  W   3	  .   �	  E   �	  ;    
     <
                  
   	                                                                                                          %s A different team  %s for different results %s %s Any questions %s contact us %s %s Developers %s and designers %s %s Each client %s a new partner %s %s Enjoy and be with us %s on social networks %s %s I would like to meet %s other projects %s %s What solution do you need? %s A 404 error occurred Additional information An error occurred Controller Do you want more services and products? File Message No Exception available Previous exceptions Stack trace The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. The requested controller was unable to dispatch the request. We cannot determine at this time why a 404 was generated. resolves to %s Project-Id-Version: ZendSkeletonApplication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-11-03 21:46-0400
PO-Revision-Date: 2014-11-03 21:46-0400
Last-Translator: Diogo Melo <dmelo87@gmail.com>
Language-Team: ZF Contibutors <zf-devteam@zend.com>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: .
X-Generator: Poedit 1.6.9
X-Poedit-SearchPath-0: ..
 %s Uma equipe diferente  %s para resultados diferentes %s %s Qualquer dúvida %s fale conosco %s %s Desenvolvedores %s e projetistas %s %s Cada cliente %s um novo parceiro %s %s Aproveite e esteja %s conosco nas redes sociais %s %s Gostaria de conhecer %s outros projetos %s %s Qual solução você precisa? %s Ocorreu um erro 404 Informação adicional Ocorreu um erro Controlador Gostaria de mais produtos e serviços? Arquivo Mensagem Nenhuma exceção disponível Exceções anteriores Pilha de execução A URL requisitada não pode ser encontrada em uma rota. O controlador requisitados não pode ser mapeado a uma classe de controlador existente. O controlador requisitado não foi despachado. O controlador requisitado não foi capaz de despachar a requisição. Não foi possível determinar o motivo do 404 ter ocorrido. resolve como %s 