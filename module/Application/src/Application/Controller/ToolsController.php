<?php
namespace Application\Controller;

use Application\Model\ToolCollection;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class ToolsController extends AbstractActionController
{
  public function listAction()
  {
    $list = array();

    $contratos = new ToolCollection('Contratos');
    $contratos->add('SABEMI', 'https://representanteonline.sabemi.com.br/Login.aspx?ReturnUrl=%2fMapaProducao.aspx');
    $contratos->add('AGIPLAN', 'http://genesis.agiplan.com.br/AgiplanNetcN/calogin.aspx');
    $contratos->add('Banco Itaú BMG', 'https://www.ibconsigweb.com.br/');
    $contratos->add('Banco BMG', 'https://www.bmgconsig.com.br/');
    $contratos->add('CIFRAS (Federal)', 'https://www.cifraconsig.com.br/Index.do?method=prepare');
    $contratos->add('Cenasp', 'http://www2.cenasp.org.br/sv_login.php?acao=100');
    $contratos->add('Caspeb', 'http://www.caspeb.com.br/');
    $contratos->add('Casebras', 'http://www.casebras.com.br/');
    $contratos->add('Banco PAN', 'https://www.pancred.com.br/');
    $contratos->add('Banrisul', 'https://consignado.bemvindobanrisul.com.br/Login.aspx?ReturnUrl=%2fDefault.aspx');
    $contratos->add('PAN Seguros', 'https://panseguros.onibusiness.com.br/');
    $contratos->sort();

    $margem = new ToolCollection('Margem');
    $margem->add('INSS', 'http://www.consultefacil.info/');
    $margem->add('Servidor Estadual', 'http://www.ceconro.com.br/d/users/sign_in');
    $margem->add('Servidor Federal', 'https://www.portalitaubmg.com.br/');
    $margem->add('Servidor Municipal (BMG Card)', 'http://177.100.20.251/signaweb/faces/index.xhtml');

    $contracheque = new ToolCollection('Contracheque');
    $contracheque->add('INSS', 'http://www3.dataprev.gov.br/cws/contexto/hiscre/');
    $contracheque->add('Servidor Estadual', 'http://sead.sefin.ro.gov.br/AcessoServicos.aspx?ReturnUrl=%2fServicos.aspx');
    $contracheque->add('Servidor Federal', 'https://www1.siapenet.gov.br/servidor/public/pages/security/acesso.jsf');
    $contracheque->add('Pensionista Federal', 'https://www1.siapenet.gov.br/pensionista/Principal.do?method=inicioLogin');

    $vd = new ToolCollection('Sistema Vida Promotora');
    $vd->add('Webmail', 'http://webmail.vidapromotora.com.br/');
    $vd->add('Chat', 'http://parceiros.vidapromotora.com.br/client.php?locale=pt-br');
    //$vd->add('Parceiros', 'http://parceiros.vidapromotora.com.br/');
    $vd->add('Funcionários', 'http://www.consig.net/credvida/consig/index.php');
    $vd->add('Corretores', 'http://www.consig.net/credvida/corretor/index.php');
    $vd->add('Metabusca Interno', 'http://177.12.173.64/index.php');
    $vd->add('SPA PAN', 'https://spa.panamericano.com.br/Default.asp');

    return new JsonModel(
      array(
        "secferramentas" => array(
          $contratos->toArray(),
          $margem->toArray(),
          $contracheque->toArray(),
          $vd->toArray()
        )
      )
    );
  }

}
