<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
  public function indexAction()
  {
    $view = new ViewModel();
    $view->setTemplate('layout/app');
    return $view;
  }

  public function mainAction()
  {
    $view = new ViewModel();
    $view->setTemplate('application/index/index');
    return $view;
  }

  public function aboutAction()
  {
    return new ViewModel(array());
  }

  public function contactAction()
  {
    return new ViewModel(array());
  }

  public function newsAction()
  {
    return new ViewModel(array());
  }

  public function partnersAction()
  {
    return new ViewModel(array());
  }

  public function toolsAction()
  {
    return new ViewModel(array());
  }

}
