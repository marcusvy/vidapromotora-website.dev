<?php
namespace Application\Model;

use Zend\Stdlib\Hydrator;

class Tool {

  /**
   * Nome da ferramenta
   * @var string
   */
  private $name;

  /**
   * Url da ferramenta
   * @var string
   */
  private $url;

  public function __construct(array $tool){
    $hydrade = new Hydrator\ClassMethods();
    $hydrade->hydrate($tool,$this);
  }

  /**
   * @return mixed
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getUrl()
  {
    return $this->url;
  }

  /**
   * @param mixed $url
   */
  public function setUrl($url)
  {
    $this->url = $url;
  }

  public function toArray()
  {
    $hydrator = new Hydrator\ClassMethods();
    return $hydrator->extract($this);
  }

}
