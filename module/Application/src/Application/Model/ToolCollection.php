<?php
namespace Application\Model;

/**
 * Class ToolCollection
 * @package Application\Model
 */
class ToolCollection
{
  /**
   * @var Lista de Ferramentas
   */
  private $list;

  /**
   * Título da coleção
   * @var
   */
  private $title;

  /**
   * @param string $title Título da coleção
   */
  public function __construct($title='')
  {
    $this->setTitle($title);
  }
  /**
   * @return mixed
   */
  public function getList()
  {
    $list = array();
    /** @var Tool $tool */
    foreach ($this->list as $tool) {
      $list[] = $tool->toArray();
    }
    return $list;
  }

  /**
   * @param mixed $list
   */
  public function setList($list)
  {
    $this->list = $list;
    return $this;
  }

  /**
   * @return mixed
   */
  public function getTitle()
  {
    return $this->title;
  }

  /**
   * @param mixed $title
   */
  public function setTitle($title)
  {
    $this->title = $title;
    return $this;
  }

  /**
   * Adiciona ferrementas a coleção
   * @param $name Nome
   * @param $url url
   * @return Tool
   */
  public function add($name, $url)
  {
    $tool = new Tool(array(
      'name'=> $name,
      'url'=> $url
    ));
    $this->list[$name] = $tool;
    return $this;
  }

  /**
   * Ordena listagem do menor para o maior
   * @param null $sort SORT Flag
   */
  public function sort($sort=null){
    asort($this->list,$sort);
  }

  /**
   * retorna o array;
   * @return array
   */
  public function toArray()
  {
    return array(
      'title' => $this->getTitle(),
      'list' => $this->getList()
    );
  }
}
