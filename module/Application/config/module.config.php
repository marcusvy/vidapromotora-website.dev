<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
  'router' => array(
    'routes' => array(
      'app' => array(
        'type' => 'Zend\Mvc\Router\Http\Literal',
        'options' => array(
          'route' => '/',
          'defaults' => array(
            'controller' => 'Application\Controller\Index',
            'action' => 'index',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => array(
          'default' => array(
            'type' => 'Segment',
            'options' => array(
              'route' => '[:controller][/:action]',
              'constraints' => array(
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              ),
              'defaults' => array(
                '__NAMESPACE__' => 'Application\Controller',
                'controller' => 'Index'
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  'service_manager' => array(
    'abstract_factories' => array(
      'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
      'Zend\Log\LoggerAbstractServiceFactory',
    ),
    'aliases' => array(
      'translator' => 'MvcTranslator',
    ),
  ),
  'translator' => array(
    'locale' => 'pt_BR',
    'translation_file_patterns' => array(
      array(
        'type' => 'gettext',
        'base_dir' => __DIR__ . '/../language',
        'pattern' => '%s.mo',
      ),
    ),
  ),
  'controllers' => array(
    'invokables' => array(
      'Application\Controller\Index' => 'Application\Controller\IndexController',
      'Application\Controller\Tools' => 'Application\Controller\ToolsController',
    ),
  ),
  'view_manager' => array(
    'display_not_found_reason' => true,
    'display_exceptions' => true,
    'doctype' => 'HTML5',
    'not_found_template' => 'error/404',
    'exception_template' => 'error/index',
    'template_map' => array(
      'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
      'layout/app' => __DIR__ . '/../view/layout/app.phtml',
      'layout/app/header' => __DIR__ . '/../view/layout/app/header.phtml',
      'layout/app/footer' => __DIR__ . '/../view/layout/app/footer.phtml',
      'layout/app/menu' => __DIR__ . '/../view/layout/app/menu.phtml',
      'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
      'application/index/about' => __DIR__ . '/../view/application/index/about.phtml',
      'application/index/contact' => __DIR__ . '/../view/application/index/contact.phtml',
      'application/index/news' => __DIR__ . '/../view/application/index/news.phtml',
      'application/index/partners' => __DIR__ . '/../view/application/index/partners.phtml',
      'application/index/tools' => __DIR__ . '/../view/application/index/tools.phtml',
      'error/404' => __DIR__ . '/../view/error/404.phtml',
      'error/index' => __DIR__ . '/../view/error/index.phtml',
    ),
    'template_path_stack' => array(
      __DIR__ . '/../view',
    ),
  ),
  // Placeholder for console routes
  'console' => array(
    'router' => array(
      'routes' => array(),
    ),
  ),
);
